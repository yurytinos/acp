//============================================================================
// Name        : ApeiaraCSVParser.cpp
// Author      : Yury Tinos Pinheiro
// Copyright   : APEIARA PROG DE COMPUTADOR-EIRELI
// Description : CSV Parser
//============================================================================

//C++ Library
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <fstream>

//C Library
#include <stdlib.h>
#include <time.h>

//Include Library
#include "JsonFactory.h"

//Boost Library
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string/trim.hpp>

using namespace std;
using namespace boost::algorithm;
using namespace boost::filesystem;
using namespace boost::property_tree;

#define LOG(x) std::cout << "LOG >>>> " << x << std::endl;

//Variáveis Globais
stringstream Aux;
ptree properties;

int main(int argc, char *argv[])
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime (buffer, 80, "log/ACP_%Y%m%d%H%M%S.log", timeinfo);

	ofstream log(buffer);
	cout.rdbuf(log.rdbuf());

	if(argc < 3)
	{
		Aux.str("");Aux << "Não há arquivos a serem processados. Favor enviar caminhos por linha de comando!";LOG(Aux.str());
		exit(-1);
	}

	Aux.str("");Aux << "Abrindo configurações...";LOG(Aux.str());
	ini_parser::read_ini("bin/acp.ini", properties);
	Aux.str("");Aux << "Configurações abertas!";LOG(Aux.str());

	vector<string> files;
	for(int i = 1; i < argc; i += 2)
	{
		int err = 0;

		Aux.str("");Aux << "MAIN : FILE = " << argv[i] << " E TOKEN = " << argv[i+1];LOG(Aux.str());

		JsonFactory json_factory(argv[i+1]);

		ifstream in;
		in.open(argv[i]);

		string data;
		if(in.is_open())
		{
			int line = 1;
			while(true)
			{
				err = 0;

				getline(in, data);

				if(in.eof()) break;

				if(json_factory.addLine(data))
				{
					if(json_factory.parse())
					{
						line++;
					}
					else
					{
						err = 1;
						std::cout << "ERRO NA LINHA " << line << "! ARQUIVO DESCARTADO!" << std::endl;
						break;
					}
				}
				else
				{
					err = 1;
					std::cout << "ERRO NA LINHA " << line << "! ARQUIVO DESCARTADO!" << std::endl;
					break;
				}
			}
			std::cout << line << " LINHAS LIDAS!!!" << std::endl;
		}
		else
		{
			Aux.str("");Aux << "ERROR = Arquivo " << argv[i] << " não aberto!";LOG(Aux.str());
		}

		if(err) continue;

		in.close();
		json_factory.toJson(argv[i]);
	}

	return 0;
}
