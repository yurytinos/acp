CXXFLAGS =	-O2 -g -Wall -fmessage-length=0

PROJ_NAME = ApeiaraCSVParser
PROJ_DIR = src

DIR = /var/apeiara/$(PROJ_NAME)
EXEC_BIN = $(DIR)/bin
EXEC_LOG = $(DIR)/log

OBJS = $(PROJ_DIR)/$(PROJ_NAME).o
LIBS = include
TARGET = $(PROJ_DIR)/$(PROJ_NAME)

EXT_CLASS = $(LIBS)/JsonFactory.cpp $(LIBS)/Json.cpp $(LIBS)/DeliveryPoints.cpp $(LIBS)/Customers.cpp $(LIBS)/Products.cpp

$(TARGET):
	$(CXX) -o $(TARGET) $(TARGET).cpp $(EXT_CLASS) -I $(LIBS)/ -lboost_system
	#Existe diretorio BIN? Se não, cria.
	if [ ! -d bin ]; then mkdir -p bin/; fi
	#Se sim, copia o executavel para o diretorio BIN
	mv -u $(TARGET) bin/

all: $(TARGET)

install:
	if [ ! -d $(DIR) ]; then mkdir -p $(DIR)/; fi
	if [ ! -d $(EXEC_BIN) ]; then mkdir -p $(EXEC_BIN)/; fi
	if [ ! -d $(EXEC_LOG) ]; then mkdir -p $(EXEC_LOG)/; fi
	#Arquivo executavel
	cp -uR bin/$(PROJ_NAME) $(EXEC_BIN)
	#Arquivo de configuração do diretorio CFG
	cp -uR cfg/acp.ini $(EXEC_BIN)

clean:
	#Limpa pasta do Executavel, se existir
	if [ -d bin ]; then rm -r bin/; fi
