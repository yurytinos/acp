/*
 * DeliveryPoints.h
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#ifndef DELIVERYPOINTS_H_
#define DELIVERYPOINTS_H_

#include "Customers.h"
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

class DeliveryPoints
{
	std::string street_type;
	std::string street;
	std::string number;
	std::string zip;
	long latitude;
	long longitude;
	std::string obs;
	int customer_code;
	std::string customer_name;
	std::string customer_adjunct;
	int product_code;
	std::string product_name;
	int amount;

public:

	std::vector<boost::shared_ptr<Customers> > customers;

	DeliveryPoints
	(
		std::string street_type,
		std::string street,
		std::string number,
		std::string zip,
		long latitude,
		long longitude,
		std::string obs,
		int customer_code,
		std::string customer_name,
		std::string customer_adjunct,
		int product_code,
		std::string product_name,
		int amount
	);

	const std::string& getCustomerAdjunct() const {
		return customer_adjunct;
	}

	void setCustomerAdjunct(const std::string& customer_adjunct) {
		this->customer_adjunct = customer_adjunct;
	}

	int getAmount() const {
		return amount;
	}

	void setAmount(int amount) {
		this->amount = amount;
	}

	int getCustomerCode() const {
		return customer_code;
	}

	void setCustomerCode(int customerCode) {
		customer_code = customerCode;
	}

	const std::string& getCustomerName() const {
		return customer_name;
	}

	void setCustomerName(const std::string& customerName) {
		customer_name = customerName;
	}

	long getLatitude() const {
		return latitude;
	}

	void setLatitude(long latitude) {
		this->latitude = latitude;
	}

	long getLongitude() const {
		return longitude;
	}

	void setLongitude(long longitude) {
		this->longitude = longitude;
	}

	const std::string& getNumber() const {
		return number;
	}

	void setNumber(const std::string& number) {
		this->number = number;
	}

	const std::string& getObs() const {
		return obs;
	}

	void setObs(const std::string& obs) {
		this->obs = obs;
	}

	int getProductCode() const {
		return product_code;
	}

	void setProductCode(int productCode) {
		product_code = productCode;
	}

	const std::string& getProductName() const {
		return product_name;
	}

	void setProductName(const std::string& productName) {
		product_name = productName;
	}

	const std::string& getStreet() const {
		return street;
	}

	void setStreet(const std::string& street) {
		this->street = street;
	}

	const std::string& getStreetType() const {
		return street_type;
	}

	void setStreetType(const std::string& streetType) {
		street_type = streetType;
	}

	const std::string& getZip() const {
		return zip;
	}

	void setZip(const std::string& zip) {
		this->zip = zip;
	}

	int parser();

	virtual ~DeliveryPoints();
};

#endif /* DELIVERYPOINTS_H_ */
