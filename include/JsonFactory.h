/*
 * JsonFactory.h
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#ifndef JSONFACTORY_H_
#define JSONFACTORY_H_

#include "Json.h"
#include "DeliveryPoints.h"
#include "Customers.h"
#include "Products.h"

#include <string>
#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/property_tree/ptree.hpp>

enum CSV
{
	itinerary = 11,
	distribution_center_code = 10,
	street_type = 3,
	street = 4,
	number = 5,
	zip = 6,
	latitude = 1,
	longitude = 2,
	obs = 13,
	customer_code = 12,
	customer_name = 14,
	adjunct = 15,
	product_code = 7,
	product_name = 9,
	amount = 8
};

class JsonFactory
{
	boost::property_tree::ptree csv_info;

	std::string token;
	std::vector<std::string> csv_cols;
	std::map<std::string, std::string> csv_line;

	std::vector<boost::shared_ptr<Json> > json;

public:
	JsonFactory(std::string token);

	int addLine(std::string line);

	int parse();

	void toJson(std::string filepath);

	virtual ~JsonFactory();
};

#endif /* JSONFACTORY_H_ */
