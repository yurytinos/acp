/*
 * Customers.cpp
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#include "Customers.h"
#include "Products.h"
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

Customers::Customers
(
	int customer_code,
	std::string customer_name,
	std::string customer_adjunct,
	int product_code,
	std::string product_name,
	int amount
)
{
	this->customer_code = customer_code;
	this->customer_name = customer_name;
	this->customer_adjunct = customer_adjunct;
	this->product_code = product_code;
	this->product_name = product_name;
	this->amount = amount;
}

int Customers::parser()
{
	try
	{
		boost::shared_ptr<Products> products;

		BOOST_FOREACH(boost::shared_ptr<Products> prod, this->products)
		{
			if
			(
				prod->getProductCode() == this->product_code &&
				prod->getProductName() == this->product_name &&
				prod->getAmount() == this->amount
			)
			{
				products = prod;
				break;
			}
		}

		if(!products)
		{
			products.reset(new Products
			(
				this->product_code,
				this->product_name,
				this->amount
			));

			this->products.push_back(products);
		}
	}
	catch(std::exception &e)
	{
		std::cout << "ERRO AO CRIAR HIERARQUIA! ERR = " << e.what() << std::endl;
		return 0;
	}

	return 1;
}

Customers::~Customers() {  }

