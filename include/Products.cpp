/*
 * Products.cpp
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#include "Products.h"
#include <string>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

Products::Products
(
	int product_code,
	std::string product_name,
	int amount
)
{
	this->product_code = product_code;
	this->product_name = product_name;
	this->amount = amount;
}

int Products::parser()
{
	return 1;
}

Products::~Products(){  }
