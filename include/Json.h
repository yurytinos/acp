/*
 * Json.h
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#ifndef Json_H_
#define Json_H_

#include "DeliveryPoints.h"
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

class Json
{
	std::string itinerary;
	std::string company_token;
	int distribution_center_code;
	std::string street_type;
	std::string street;
	std::string number;
	std::string zip;
	long latitude;
	long longitude;
	std::string obs;
	int customer_code;
	std::string customer_name;
	std::string customer_adjunct;
	int product_code;
	std::string product_name;
	int amount;

public:

	std::vector<boost::shared_ptr<DeliveryPoints> > delivery_points;

	Json
	(
		std::string itinerary,
		std::string company_token,
		int distribution_center_code,
		std::string street_type,
		std::string street,
		std::string number,
		std::string zip,
		long latitude,
		long longitude,
		std::string obs,
		int customer_code,
		std::string customer_name,
		std::string customer_adjunct,
		int product_code,
		std::string product_name,
		int amount
	);

	const std::string& getCustomerAdjunct() const {
		return customer_adjunct;
	}

	void setCustomerAdjunct(const std::string& customer_adjunct) {
		this->customer_adjunct = customer_adjunct;
	}

	int getAmount() const {
		return amount;
	}

	void setAmount(int amount) {
		this->amount = amount;
	}

	const std::string& getCompanyToken() const {
		return company_token;
	}

	void setCompanyToken(const std::string& companyToken) {
		company_token = companyToken;
	}

	int getCustomerCode() const {
		return customer_code;
	}

	void setCustomerCode(int customerCode) {
		customer_code = customerCode;
	}

	const std::string& getCustomerName() const {
		return customer_name;
	}

	void setCustomerName(const std::string& customerName) {
		customer_name = customerName;
	}

	int getDistributionCenterCode() const {
		return distribution_center_code;
	}

	void setDistributionCenterCode(int distributionCenterCode) {
		distribution_center_code = distributionCenterCode;
	}

	const std::string& getItinerary() const {
		return itinerary;
	}

	void setItinerary(const std::string& itinerary) {
		this->itinerary = itinerary;
	}

	long getLatitude() const {
		return latitude;
	}

	void setLatitude(long latitude) {
		this->latitude = latitude;
	}

	long getLongitude() const {
		return longitude;
	}

	void setLongitude(long longitude) {
		this->longitude = longitude;
	}

	const std::string& getNumber() const {
		return number;
	}

	void setNumber(const std::string& number) {
		this->number = number;
	}

	const std::string& getObs() const {
		return obs;
	}

	void setObs(const std::string& obs) {
		this->obs = obs;
	}

	int getProductCode() const {
		return product_code;
	}

	void setProductCode(int productCode) {
		product_code = productCode;
	}

	const std::string& getProductName() const {
		return product_name;
	}

	void setProductName(const std::string& productName) {
		product_name = productName;
	}

	const std::string& getStreet() const {
		return street;
	}

	void setStreet(const std::string& street) {
		this->street = street;
	}

	const std::string& getStreetType() const {
		return street_type;
	}

	void setStreetType(const std::string& streetType) {
		street_type = streetType;
	}

	const std::string& getZip() const {
		return zip;
	}

	void setZip(const std::string& zip) {
		this->zip = zip;
	}

	int parser();

	virtual ~Json();
};

#endif /* Json_H_ */
