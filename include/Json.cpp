/*
 * Json.cpp
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#include "Json.h"
#include "DeliveryPoints.h"
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

Json::Json
(
	std::string itinerary,
	std::string company_token,
	int distribution_center_code,
	std::string street_type,
	std::string street,
	std::string number,
	std::string zip,
	long latitude,
	long longitude,
	std::string obs,
	int customer_code,
	std::string customer_name,
	std::string customer_adjunct,
	int product_code,
	std::string product_name,
	int amount
)
{
	this->itinerary = itinerary;
	this->company_token = company_token;
	this->distribution_center_code = distribution_center_code;
	this->street_type = street_type;
	this->street = street;
	this->number = number;
	this->zip = zip;
	this->latitude = latitude;
	this->longitude = longitude;
	this->obs = obs;
	this->customer_code = customer_code;
	this->customer_name = customer_name;
	this->customer_adjunct = customer_adjunct;
	this->product_code = product_code;
	this->product_name = product_name;
	this->amount = amount;
}

int Json::parser()
{
	try
	{
		int err = 1;

		boost::shared_ptr<DeliveryPoints> delivery_points;

		BOOST_FOREACH(boost::shared_ptr<DeliveryPoints> dp , this->delivery_points)
		{
			if
			(
				dp->getStreetType() == this->street_type &&
				dp->getStreet() == this->street &&
				dp->getNumber() == this->number &&
				dp->getZip() == this->zip &&
				dp->getLatitude() == this->latitude &&
				dp->getLongitude() == this->longitude &&
				dp->getObs() == this->obs
			)
			{
				delivery_points = dp;

				delivery_points->setCustomerCode(this->customer_code);
				delivery_points->setCustomerName(this->customer_name);
				delivery_points->setCustomerAdjunct(this->customer_adjunct);
				delivery_points->setProductCode(this->product_code);
				delivery_points->setProductName(this->product_name);
				delivery_points->setAmount(this->amount);

				err = delivery_points->parser();

				break;
			}
		}

		if(!delivery_points)
		{
			delivery_points.reset(new DeliveryPoints
			(
				this->street_type,
				this->street,
				this->number,
				this->zip,
				this->latitude,
				this->longitude,
				this->obs,
				this->customer_code,
				this->customer_name,
				this->customer_adjunct,
				this->product_code,
				this->product_name,
				this->amount
			));

			this->delivery_points.push_back(delivery_points);

			err = delivery_points->parser();
		}

		return err;
	}
	catch(std::exception &e)
	{
		std::cout << "ERRO AO CRIAR HIERARQUIA! ERR = " << e.what() << std::endl;
		return 0;
	}

	return 0;
}

Json::~Json() {  }

