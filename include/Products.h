/*
 * Products.h
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#ifndef PRODUCTS_H_
#define PRODUCTS_H_

#include <string>

class Products
{
	int product_code;
	std::string product_name;
	int amount;

public:
	Products
	(
		int product_code,
		std::string product_name,
		int amount
	);

	int getAmount() const {
		return amount;
	}

	void setAmount(int amount) {
		this->amount = amount;
	}

	int getProductCode() const {
		return product_code;
	}

	void setProductCode(int productCode) {
		product_code = productCode;
	}

	const std::string& getProductName() const {
		return product_name;
	}

	void setProductName(const std::string& productName) {
		product_name = productName;
	}

	int parser();

	virtual ~Products();
};

#endif /* PRODUCTS_H_ */
