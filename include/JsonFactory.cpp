/*
 * JsonFactory.cpp
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#include "JsonFactory.h"
#include "Json.h"
#include "DeliveryPoints.h"
#include "Customers.h"
#include "Products.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <exception>

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

JsonFactory::JsonFactory(std::string token)
{
	this->token = token;

	boost::property_tree::ini_parser::read_ini("bin/acp.ini", this->csv_info);

	std::string cols_str = this->csv_info.get<std::string>("CSV.Cols");
	boost::algorithm::split(this->csv_cols, cols_str, boost::is_any_of(","));

	this->csv_line.clear();
	for(unsigned int i = 0; i < this->csv_cols.size(); i++)
	{
		this->csv_line.insert(std::pair<std::string, std::string>(this->csv_cols[i],""));
	}
}

int JsonFactory::addLine(std::string line)
{
	try
	{

		std::vector<std::string> columns;
		boost::algorithm::split(columns, line, boost::is_any_of(";"));

		for(unsigned int i = 0; i < this->csv_cols.size(); i++)
		{
			boost::algorithm::trim(columns[i]);
			this->csv_line.at(this->csv_cols[i]) = columns[i];
		}

		if
		(
			this->csv_line.at("itinerary").empty() ||
			this->token.empty() ||
			this->csv_line.at("distribution_center_code").empty() ||
			this->csv_line.at("street_type").empty() ||
			this->csv_line.at("street").empty() ||
			this->csv_line.at("number").empty() ||
			this->csv_line.at("customer_name").empty() ||
			this->csv_line.at("adjunct").empty() ||
			this->csv_line.at("product_name").empty() ||
			this->csv_line.at("amount").empty()
		)
		{
			return 0;
		}
	}
	catch(std::exception const&  e)
	{
		std::cout << "ERRO AO CRIAR HIERARQUIA!! NUMERO INDEVIDO DE COLUNAS! ERR = " << e.what() << std::endl;
		return 0;
	}

	return 1;
}

int JsonFactory::parse()
{
	try
	{
		int err = 1;

		boost::shared_ptr<Json> json;

		BOOST_FOREACH(boost::shared_ptr<Json> js , this->json)
		{
			if
			(
				js->getItinerary() == this->csv_line["itinerary"] &&
				js->getCompanyToken() == this->token &&
				js->getDistributionCenterCode() == atoi(this->csv_line["distribution_center_code"].c_str())
			)
			{
				json = js;

				json->setStreetType(this->csv_line["street_type"]);
				json->setStreet(this->csv_line["street"]);
				json->setNumber(this->csv_line["number"]);
				json->setZip(this->csv_line["zip"]);
				json->setLatitude(atol(this->csv_line["latitude"].c_str()));
				json->setLongitude(atol(this->csv_line["longitude"].c_str()));
				json->setObs(this->csv_line["obs"]);
				json->setCustomerCode(atoi(this->csv_line["customer_code"].c_str()));
				json->setCustomerName(this->csv_line["customer_name"]);
				json->setCustomerAdjunct(this->csv_line["adjunct"]);
				json->setProductCode(atoi(this->csv_line["product_code"].c_str()));
				json->setProductName(this->csv_line["product_name"]);
				json->setAmount(atoi(this->csv_line["amount"].c_str()));

				err = json->parser();
				break;
			}
		}

		if(!json)
		{
			json.reset(new Json
			(
				this->csv_line["itinerary"],
				this->token,
				atoi(this->csv_line["distribution_center_code"].c_str()),
				this->csv_line["street_type"],
				this->csv_line["street"],
				this->csv_line["number"],
				this->csv_line["zip"],
				atol(this->csv_line["latitude"].c_str()),
				atol(this->csv_line["longitude"].c_str()),
				this->csv_line["obs"],
				atoi(this->csv_line["customer_code"].c_str()),
				this->csv_line["customer_name"],
				this->csv_line["adjunct"],
				atoi(this->csv_line["product_code"].c_str()),
				this->csv_line["product_name"],
				atoi(this->csv_line["amount"].c_str())
			));

			this->json.push_back(json);

			err = json->parser();
		}

		return err;
	}
	catch(std::exception const&  e)
	{
		std::cout << "ERRO AO CRIAR HIERARQUIA! ERR = " << e.what() << std::endl;
		return 0;
	}

	return 1;
}

void JsonFactory::toJson(std::string filepath)
{
	std::stringstream path;
	path.str("");
	path << filepath << ".json";

	std::ofstream temp;
	temp.open(path.str().c_str(), std::ofstream::out | std::ofstream::trunc);

	temp << "[ ";
	int x = 0;
	BOOST_FOREACH(boost::shared_ptr<Json> js , this->json)
	{
		temp << "{ ";
		temp << "\"itinerary\" : \"" << js->getItinerary() << "\",";
		temp << "\"company_token\" : \"" << js->getCompanyToken() << "\",";
		temp << "\"distribution_center_code\" : \"" << js->getDistributionCenterCode() << "\",";
		temp << "\"delivery_points\" : ";
			temp << "[ ";
			int i = 0;
			BOOST_FOREACH(boost::shared_ptr<DeliveryPoints> dp , js->delivery_points)
			{
				temp << "{ ";
				temp << "\"street_type\" : \"" << dp->getStreetType() << "\",";
				temp << "\"street\" : \"" << dp->getStreet() << "\",";
				temp << "\"number\" : \"" << dp->getNumber() << "\",";
				temp << "\"zip\" : \"" << dp->getZip() << "\",";
				temp << "\"latitude\" : \"" << dp->getLatitude() << "\",";
				temp << "\"longitude\" : \"" << dp->getLongitude() << "\",";
				temp << "\"obs\" : \"" << dp->getObs() << "\",";
				temp << "\"customers\" : ";
					temp << "[ ";
					int j = 0;
					BOOST_FOREACH(boost::shared_ptr<Customers> cus, dp->customers)
					{
						temp << "{ ";
						temp << "\"code\" : \"" << cus->getCustomerCode() << "\",";
						temp << "\"name\" : \"" << cus->getCustomerName() << "\",";
						temp << "\"adjunct\" : \"" << cus->getCustomerAdjunct() << "\",";
						temp << "\"products\" : ";
							temp << "[ ";
							int k = 0;
							BOOST_FOREACH(boost::shared_ptr<Products> prod, cus->products)
							{
								temp << "{ ";
								temp << "\"code\" : \"" << prod->getProductCode() << "\",";
								temp << "\"name\" : \"" << prod->getProductName() << "\",";
								temp << "\"amount\" : \"" << prod->getAmount() << "\"";

								k++;
								if(k < cus->products.size()) temp << " }, ";
								else temp << " } ";
							}
							temp << " ] ";

						j++;
						if(j < dp->customers.size()) temp << " }, ";
						else temp << " } ";
					}
					temp << " ] ";

				i++;
				if(i < js->delivery_points.size()) temp << " }, ";
				else temp << " } ";
			}
			temp << " ] ";

		x++;
		if(x < this->json.size()) temp << "}, ";
		else temp << " } ";
	}
	temp << " ]";

	temp.close();
}

JsonFactory::~JsonFactory() {  }

