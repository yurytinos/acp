/*
 * Customers.h
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#ifndef CUSTOMERS_H_
#define CUSTOMERS_H_

#include "Products.h"
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

class Customers
{
	int customer_code;
	std::string customer_name;
	std::string customer_adjunct;
	int product_code;
	std::string product_name;
	int amount;

public:

	std::vector<boost::shared_ptr<Products> > products;

	Customers
	(
		int customer_code,
		std::string customer_name,
		std::string customer_adjunct,
		int product_id,
		std::string product_name,
		int amount
	);

	const std::string& getCustomerAdjunct() const {
		return customer_adjunct;
	}

	void setCustomerAdjunct(const std::string& customer_adjunct) {
		this->customer_adjunct = customer_adjunct;
	}

	int getAmount() const {
		return amount;
	}

	void setAmount(int amount) {
		this->amount = amount;
	}

	int getCustomerCode() const {
		return customer_code;
	}

	void setCustomerCode(int customerCode) {
		customer_code = customerCode;
	}

	const std::string& getCustomerName() const {
		return customer_name;
	}

	void setCustomerName(const std::string& customerName) {
		customer_name = customerName;
	}

	int getProductCode() const {
		return product_code;
	}

	void setProductCode(int productCode) {
		product_code = productCode;
	}

	const std::string& getProductName() const {
		return product_name;
	}

	void setProductName(const std::string& productName) {
		product_name = productName;
	}

	int parser();

	virtual ~Customers();
};

#endif /* CUSTOMERS_H_ */
