/*
 * DeliveryPoints.cpp
 *
 *  Created on: 19/08/2013
 *      Author: yury
 */

#include "DeliveryPoints.h"
#include "Customers.h"
#include <iostream>
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

DeliveryPoints::DeliveryPoints
(
	std::string street_type,
	std::string street,
	std::string number,
	std::string zip,
	long latitude,
	long longitude,
	std::string obs,
	int customer_code,
	std::string customer_name,
	std::string customer_adjunct,
	int product_code,
	std::string product_name,
	int amount
)
{
	this->street_type = street_type;
	this->street = street;
	this->number = number;
	this->zip = zip;
	this->latitude = latitude;
	this->longitude = longitude;
	this->obs = obs;
	this->customer_code = customer_code;
	this->customer_name = customer_name;
	this->customer_adjunct = customer_adjunct;
	this->product_code = product_code;
	this->product_name = product_name;
	this->amount = amount;
}

int DeliveryPoints::parser()
{
	try
	{
		int err = 1;

		boost::shared_ptr<Customers> customers;

		BOOST_FOREACH(boost::shared_ptr<Customers> cus , this->customers)
		{
			if
			(
				cus->getCustomerCode() == this->customer_code &&
				cus->getCustomerName() == this->customer_name &&
				cus->getCustomerAdjunct() == this->customer_adjunct
			)
			{
				cus->setProductCode(this->product_code);
				cus->setProductName(this->product_name);
				cus->setAmount(this->amount);

				err = cus->parser();

				customers = cus;

				break;
			}
		}

		if(!customers)
		{
			customers.reset(new Customers
			(
				this->customer_code,
				this->customer_name,
				this->customer_adjunct,
				this->product_code,
				this->product_name,
				this->amount
			));

			err = customers->parser();

			this->customers.push_back(customers);
		}

		return err;
	}
	catch(std::exception &e)
	{
		std::cout << "ERRO AO CRIAR HIERARQUIA! ERR = " << e.what() << std::endl;
		return 0;
	}

	return 1;
}

DeliveryPoints::~DeliveryPoints(){  }

