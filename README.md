# Apeiara CSV Parser
 
## Parser CSV para JSON padrão Apeiara

**ZAVAM** 
```bash

/var/apeiara/ApeiaraCSVParser/bin/ApeiaraCSVParser <params...>*

params = [ <file_path> & <company_token> ]*

Ex.: .../bin/ApeiaraCSVParser FILE1.csv TOKEN1 FILE2.csv TOKEN2

Após a execução, serão criados os arquivos *.json no mesmo 
diretório dos arquivos originais.

Diretórios
|
|-> bin -- contem o executável e o arquivo de configuração do projeto
|-> log -- contem os logs de execução do parser. Ex.: ACP_AAMMDDHHMISS.log

```